var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectId;
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

/* heler functions */
  function userToFriend(user){
	return {
		id: user.id,
		name: user.name,
		online: user.loged_in,
		};
  }

  async function getFriend(id, dbo){
	  let query = {'id': id };
	  let fren = await dbo.collection("users").find(query).toArray();
  	  if (fren.length > 0){
	 	 return userToFriend(fren[0]);
	  }
	  else{
	  	return {error: "user not found"};
	  }
  }

  function resetFriends(user){
	let new_friends = [];
	for (friend of user.friends){
		new_friends.push(friend.id)
	}
	for (friend of user.invited_friends){
		new_friends.push(friend.id)
	}
	delete user.requested_friends;
	delete user.invited_friends;
	user.friends = new_friends;
	return user;
  }

  function escapeRegExp(string) {
	  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
  }

/* GET users listing.*/ 

router.get('/mychats', async function(req, res, next) {
	let client = await MongoClient.connect(url);
	let dbo = client.db("ytpdb");
	if(! req.query.user){
		res.json({error: "invalid data"});
		next();
		return;
	}
	let user = JSON.parse(req.query.user);
	let query = { name: user.name, password: user.password };
	let users = await dbo.collection("users").find(query).toArray();
	console.log(users);
	if (users.length == 0){
		res.json({Error: "invalid credentials"});
		next();
		return;
	}
	user = users[0];
	query = { chaters: { $elemMatch: {$in: [user.id] }}};
	let chats = await dbo.collection("chats").find(query).toArray();
	console.log(chats);
	for (let chat of chats){
		let new_chaters = [];
		for (let friend of chat.chaters){
			let fren = await getFriend(friend, dbo);
			console.log(fren);
			new_chaters.push(fren);
		}
		chat.chaters = new_chaters;
	}
	res.json({"chats": JSON.stringify(chats)});
});


/* put chat listening*/
router.put('/create', async function(req, res, next) {
	let client = await MongoClient.connect(url);
	let dbo = client.db("ytpdb");
	console.log(req.body);
	if(! req.body.user && ! req.body.chat){
		res.json({error: "invalid data"});
		next();
		return;
	}
	let user = JSON.parse(req.body.user);
	let chat = JSON.parse(req.body.chat);
	delete chat.id;
	chat.messages = [];
	console.log(user);
	let query = { name: user.name, password: user.password };
	let users = await dbo.collection("users").find(query).toArray();
	console.log(users);
	if (users.length == 0){
		res.json({error: "invalid credentials"});
		next();
		return;
	}
	user = users[0];
	let new_chaters = [];
	for (let chater of chat.chaters){
		if (! chater in user.friends.concat(user.id)){
			res.json({error: "can't invite non-friends"});
			next();
			return;
		}
		new_chaters.push(chater.id);
	}
	chat.chaters = new_chaters;
	let response = await dbo.collection("chats").insertOne(chat);
	console.log(response);
	res.json({result: "OK"});
});

/* post a chat request */
router.post('/updatechat', async function(req, res, next) {
	let client = await MongoClient.connect(url);
        let dbo = client.db("ytpdb");
        console.log(req.body);
        if(! req.body.user || ! req.body.chat){
                res.json({error: "invalid data"});
                next();
                return;
        }
        let user = JSON.parse(req.body.user);
        let chat = JSON.parse(req.body.chat);
        console.log(user);
        console.log(chat);
        let query = { name: user.name, password: user.password };
        let users = await dbo.collection("users").find(query).toArray();
        console.log(users);
        if (users.length == 0){
                res.json({error: "invalid credentials"});
                next();
                return;
        }
	user = users[0];
	for (let friend of chat.chaters){
		if (! friend.id in user.friends){
			res.json({error: "can't update chat with people who are not your friends"});
			next();
			return;
		}
	}
	chat.chaters = chat.chaters.map((x)=>{return x.id}); 
	query = {"_id": new ObjectId(chat.id)};
	console.log(query);
	let update = await dbo.collection("chats").replaceOne(query, chat);
	console.log(update);
	res.json({result: "OK"});

});


router.post('/postmessage', async function(req, res, next) {
	let client = await MongoClient.connect(url);
	let dbo = client.db("ytpdb");
        console.log(req.body);
        if(! req.body.user || ! req.body.message){
                res.json({error: "invalid data"});
                next();
                return;
        }
        let user = JSON.parse(req.body.user);
        let message = req.body.message;
	let chat_id = req.body.chat_id;
        console.log(user);
        console.log(message);
        let query = { name: user.name, password: user.password };
	let users = await dbo.collection("users").find(query).toArray();
	console.log(users);
	if (users.length == 0){
		res.json({Error: "invalid credentials"});
		next();
		return;
	}
	user = users[0];
	query = {"_id": new ObjectId(chat_id), "chaters": {$elemMatch: { $eq:  user.id}}};
	chat = await dbo.collection("chats").findOne(query);
	if (! chat){
		res.json({Error: "invalid credentials"});
		next();
		return;
	}
	let post = {"poster": user.name, "content": message, "date": Date()}; 
	let update = {$push: {"messages": post}}
	let result = await dbo.collection("chats").updateOne(query, update);
	console.log(result);
	res.json({result: "OK"});
});

/* delete a chat request */
router.delete('/delete', async function(req, res, next) {
	let client = await MongoClient.connect(url);
	let dbo = client.db("ytpdb");
	console.log(req.body);
	if(! req.query.user || ! req.query.chat_id){
		res.json({error: "invalid data"});
		next();
		return;
	}
	let user = JSON.parse(req.query.user);
	let chat_id = req.query.chat_id;
	console.log(user);
	console.log(chat_id);
	let query = { name: user.name, password: user.password };
	let users = await dbo.collection("users").find(query).toArray();
	console.log(users);
	if (users.length == 0){
		res.json({error: "invalid credentials"});
		next();
		return;
	}
	user = users[0];
	query = {"_id": new ObjectId(chat_id), "chaters": {$elemMatch: { $eq:  user.id}}};
	console.log(query);
	let deleted = await dbo.collection("chats").remove(query, true);
	console.log(deleted);
	res.json({result: "OK"});
});

module.exports = router;
