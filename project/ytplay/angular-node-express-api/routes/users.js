var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

/* heler functions */
  function userToFriend(user){
	return {
		id: user.id,
		name: user.name,
		online: user.loged_in,
		};
  }

  async function getFriend(id, dbo){
	  let query = {'id': id };
	  let fren = await dbo.collection("users").find(query).toArray();
  	  if (fren.length > 0){
	 	 return userToFriend(fren[0]);
	  }
	  else{
	  	return {error: "user not found"};
	  }
  }

  function resetFriends(user){
	let new_friends = [];
	for (friend of user.friends){
		new_friends.push(friend.id)
	}
	for (friend of user.invited_friends){
		new_friends.push(friend.id)
	}
	delete user.requested_friends;
	delete user.invited_friends;
	user.friends = new_friends;
	return user;
  }

  function escapeRegExp(string) {
	  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
  }

/* GET users listing.*/ 

router.get('/login', async function(req, res, next) {
	let client = await MongoClient.connect(url);
	console.log(client);
	let dbo = client.db("ytpdb");
	console.log(req.query);
	let query = { name: req.query.name, password: req.query.password };
	let users = await dbo.collection("users").find(query).toArray();
	console.log(users);
	if (users.length == 0){
		res.json({});
		next();
		return;
	}
	let user = users[0];
	console.log(user);
	query = { friends: { $elemMatch: { $eq: user.id }}};
	let friends = await dbo.collection("users").find(query).toArray();;
	console.log(friends);
	let new_friends = [];
	user["requested_friends"] = [];
	user["invited_friends"] = [];
	for (let friend of friends){
		let fren = userToFriend(friend);
		console.log(friend.id);
		console.log(user.friends);
		console.log(friend.id in user.friends);
		if (user.friends.indexOf(friend.id) != -1){
			let index = user.friends.indexOf(friend.id);
			user.friends.splice(index,1);
			new_friends.push(fren);
		}
		else{
			user.requested_friends.push(fren);
		}
	}
	for (let id of user.friends){
		user.invited_friends.push(await getFriend(id, dbo));
	}
	user.friends = new_friends;
	user.loged_in = 1;
	res.json(user);
});

/* get list of friends **/
router.get('/friendsearch', async function(req,res, next){
	let client = await MongoClient.connect(url);
	let dbo = client.db("ytpdb");
	let escaped = escapeRegExp(req.query.search)
	let search_regex = new RegExp(escaped);
	let query = { name: {$regex: search_regex} };
	let friends = await dbo.collection("users").find(query).toArray();
	let result = [];
	for (let friend of friends){
		let fren = userToFriend(friend);
		result.push(fren);
	}
	res.json({search_result: result});
});

/* put user listening*/
router.put('/update', async function(req, res, next) {
	let client = await MongoClient.connect(url);
	let dbo = client.db("ytpdb");
	console.log(req.body);
	if(! req.body.user){
		res.json({error: "invalid data"});
		next();
		return;
	}
	let user = JSON.parse(req.body.user);
	console.log(user);
	let query = { name: user.name, password: user.password };
	let users = await dbo.collection("users").find(query).toArray();
	console.log(users);
	if (users.length == 0){
		res.json({error: "invalid credentials"});
		next();
		return;
	}
	user = resetFriends(user);
	let update = await dbo.collection("users").replaceOne(query, user);
	console.log(update)
	res.json({result: "OK"});
});

/* deny a friend request */
router.put('/denyfriend', async function(req, res, next) {
	let client = await MongoClient.connect(url);
	let dbo = client.db("ytpdb");
	console.log(req.body);
	if(! req.body.user || ! req.body.friend){
		res.json({error: "invalid data"});
		next();
		return;
	}
	let user = JSON.parse(req.body.user);
	let friend_to_deny = JSON.parse(req.body.friend);
	console.log(user);
	console.log(friend_to_deny);
	let query = { name: user.name, password: user.password };
	let users = await dbo.collection("users").find(query).toArray();
	console.log(users);
	if (users.length == 0){
		res.json({error: "invalid credentials"});
		next();
		return;
	}
	user = users[0];
	query = {id: friend_to_deny.id};
	let new_values = {$pull: {friends: user.id}}
	let update = await dbo.collection("users").updateOne(query, new_values);
	console.log(update)
	res.json({result: "OK"});
});

module.exports = router;
