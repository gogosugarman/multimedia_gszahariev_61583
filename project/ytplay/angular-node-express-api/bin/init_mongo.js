#!/usr/bin/env node

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("ytpdb");
  dbo.createCollection("users", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
});

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("ytpdb");
  dbo.createCollection("chats", function(err, res) {
    if (err) throw err;
    console.log("Collection created!");
    db.close();
  });
});

MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db("ytpdb");
  var myobj = [
	{
		id: 1,
		name: 'admin',
		password: 'cokemock',
		loged_in: 0,
		auth_token: '',
		friends: [2,3,4],
	},
	{
		id: 2,
		name: 'regular',
		password: 'cokemock',
		loged_in: 0,
		friends: [1],
		auth_token: ''
	},
	{
		id: 3,
		name: 'joey',
		password: 'cokemock',
		loged_in: 0,
		friends: [1],
		auth_token: ''
	},
	{
		id: 4,
		name: 'Al',
		password: 'weird',
		loged_in: 0,
		friends: [1],
		auth_token: ''
	}
	
  ];
  dbo.collection("users").insertMany(myobj, function(err, res) {
    if (err) throw err;
    console.log("Number of documents inserted: " + res.insertedCount);
    db.close();
  });
});
