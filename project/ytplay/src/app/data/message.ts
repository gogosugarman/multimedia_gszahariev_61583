export class Message{
	date: string;
	content: string;
	poster: string;
	constructor(obj){
		this.date = obj.date;
		this.content = obj.content;
		this.poster = obj.poster;
	}
}
