export class Friend {
	id: number;
	name: string;
	online: number;
	constructor(obj){
		this.id = obj.id;
		this.name = obj.name;
		this.online = 1;
	}
}
