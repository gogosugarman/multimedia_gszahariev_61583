export class Video{
	id: string;
	video_title: string;
	channel_id: string;
	channel_name: string;
	thumb_def: string;
	thumb_med: string;
	thumb_high: string;
	thumb_def_w: number;
	thumb_def_h: number;
	thumb_med_w: number;
	thumb_med_h: number;
	thumb_high_w: number;
	thumb_high_h: number;
	playable: number;
	description: string;

	constructor(
		id: string,
		video_title: string,
		channel_id: string,
		channel_name: string,
		thumb_def: string,
		thumb_med: string,
		thumb_high: string,
		thumb_def_w: number,
		thumb_def_h: number,
		thumb_med_w: number,
		thumb_med_h: number,
		thumb_high_w: number,
		thumb_high_h: number,
		description: string,
		)
		{
	
		this.id = id;
		this.video_title = video_title.replace(/&quot;/g,'"');
		this.channel_id = channel_id;
		this.channel_name = channel_name;
		this.thumb_def = thumb_def;
		this.thumb_med = thumb_med;
		this.thumb_high = thumb_high;
		this.thumb_def_w = thumb_def_w;
		this.thumb_def_h = thumb_def_h;
		this.thumb_med_w = thumb_med_w;
		this.thumb_med_h = thumb_med_h;
		this.thumb_high_w = thumb_high_w;
		this.thumb_high_h = thumb_high_h;
		this.description = description;
		this.playable = 0;
		}
}
