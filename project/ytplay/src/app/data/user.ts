import { Friend } from './friend';
export class User {
	id: number;
	name: string;
	password: string;
	loged_in: number;
	auth_token: string;
	friends: Array<Friend>;
	invited_friends: Array<Friend>;
	requested_friends: Array<Friend>;

	constructor(obj){
		this.id = obj.id;
		this.name = obj.name;
		this.password = obj.password;
		this.loged_in = obj.loged_in;
		this.auth_token = obj.auth_token;
		this.friends = obj.friends;
		this.invited_friends = obj.invited_friends;
		this.requested_friends = obj.requested_friends;
	}	
}
	
