import { Friend } from './friend';
import { Message } from './message';
export class Chat {
	id: string;
	name: string;
	chaters: Array<Friend>;
	messages: Array<Message>;
	constructor(obj){
		this.id = obj._id;
		this.name = obj.name;
		this.chaters = obj.chaters;
		this.messages = obj.messages;
	}	
}
