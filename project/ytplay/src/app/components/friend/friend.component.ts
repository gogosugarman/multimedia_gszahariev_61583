import { Component, OnInit, Input } from '@angular/core';
import { Friend } from '../../data/friend';
import { LoginService } from '../../services/login/login.service'
import { ChatService } from '../../services/chat/chat.service'

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css']
})
export class FriendComponent implements OnInit {

  @Input()
  variation: string;
  @Input()
  friend: Friend;
  expanded = 0;

  constructor(private loginService: LoginService,
	      private chatService: ChatService) { }

  ngOnInit() {
  }

  getClass(type: string){
	if(type == "friend"){
		if(this.variation == "buddy"){
			return "friend";
		}
		else if (this.variation == "to_be_buddy"){
			return "to-be-friend";
		}
		else if (this.variation == "wanna_be_buddy"){
			return "wanna-be-friend";
		}
		else if (this.variation == "addable_buddy"){
			return "addable-friend";
		}
		else if (this.variation == "chater"){
			return "friend";
		}
	}
	return "ERROR-CLASS";
  }

  toggleExpand(){
	this.expanded = (this.expanded + 1) % 2;
  }

  unfriend(){
	this.loginService.removeFriend(this.friend);
  }

  uninvite(){
	this.loginService.uninviteFriend(this.friend);
  }

  denyRequest(){
	this.loginService.denyFriendRequest(this.friend);
  }
  addFriend(){
	this.loginService.addFriend(this.friend);
  }

  addToChat(){
	this.chatService.addFriendToCurrentChat(this.friend);
  }
}
