import { Component, OnInit } from '@angular/core';
import { VideoService } from '../../services/video/video.service'
import { DragulaService } from 'ng2-dragula';
import { Video } from '../../data/video';

@Component({
  selector: 'app-related-holder',
  templateUrl: './related-holder.component.html',
  styleUrls: ['./related-holder.component.css']
})
export class RelatedHolderComponent implements OnInit {

  constructor(private videoService: VideoService,
	      private dragulaService: DragulaService) {
	dragulaService.destroy('VIDEOS');
	let videosdrag = dragulaService.createGroup('VIDEOS', {
		accepts: (el, target, source, sibling) => {
			return target.id == 'video.playlist.id';
		},
		copy: (el, source) => {
			return source.id !== 'video.playlist.id';
		},
		copyItem: (video: Video) => {
			return JSON.parse(JSON.stringify(video));
		}
	});
	videosdrag.drake.on('drag',
		(el,source) => {
			videoService.dragging = 1;
		}
	);	
	videosdrag.drake.on('dragend',
		(el,source) => {
			videoService.dragging = 0;
		}
	);	
 }

  ngOnInit() {
  }

}
