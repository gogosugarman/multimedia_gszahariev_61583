import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatedHolderComponent } from './related-holder.component';

describe('RelatedHolderComponent', () => {
  let component: RelatedHolderComponent;
  let fixture: ComponentFixture<RelatedHolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatedHolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatedHolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
