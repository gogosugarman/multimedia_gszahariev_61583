import { Component, OnInit } from '@angular/core';
import { Video } from '../../data/video';
import { VideoService } from '../../services/video/video.service'

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  private player;
  private ytEvent;
  first_time = 1;

  playerVars = {
    cc_lang_pref: 'en'
  };
  on_load_command = "none";
  constructor(private videoService: VideoService) { }

  ngOnInit() {
    this.videoService.new_player_video.subscribe( value => {
    	if (value === true) {
		if (! this.first_time){
			this.changeVideo(this.videoService.player_video['id']);
		}
		else{
			this.first_time = 0;
			console.log("first");
		}
    	}
	});
  }


  onStateChange(event) {
    this.ytEvent = event.data;
    if (event.data == 0){
	this.videoService.playerNextVideo();	
    }
  }
  savePlayer(player) {
    this.player = player;
    this.playVideo();
  }

  changeVideo(id: string){
	this.player.loadVideoById({
		      videoId: id
                      });
  }

  playVideo() {
    this.player.playVideo();
  }
  
  pauseVideo() {
    this.player.pauseVideo();
  }
  stopVideo(){
	  console.log("????");
  }

}
