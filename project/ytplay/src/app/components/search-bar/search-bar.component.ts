import { Component, OnInit } from '@angular/core';
import { VideoService  } from '../../services/video/video.service'

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  constructor(private videoService: VideoService) { }

  ngOnInit() {
  }
  search_text: String = ""; 
 
  search(keyEvent: KeyboardEvent){
	if(keyEvent.keyCode === 13){
		this.videoService.search(this.search_text);
		this.search_text = "";
	}
  }
}
