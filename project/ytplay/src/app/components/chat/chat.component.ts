import { Component, OnInit, Input } from '@angular/core';
import { Chat } from '../../data/chat';
import { Friend } from '../../data/friend';
import { ChatService } from '../../services/chat/chat.service'
import { LoginService } from '../../services/login/login.service'

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  constructor(private chatService: ChatService,
	      private loginService: LoginService) { }

  ngOnInit() {
  }
  @Input()
  chat: Chat;
  chat_expanded = 0;
  chat_renaming = 0;
  tmp_name = "";

  getChatClass(chat: Chat){
	if(this.chatService.current_chat &&
	   chat.id == this.chatService.current_chat.id){
		return "chat-selected";
	}
	else{
		return "chat";
	}
  }

  toggleExpand(){
	this.chat_expanded = (this.chat_expanded + 1) % 2;
  }

  setCurrentChat(chat: Chat){
	this.chatService.setCurrentChat(chat);
  }

  delete(){
	this.chatService.deleteChat(this.chat);
  }

  rename(){
	this.chat_renaming = 1;
	this.chat_expanded = 0;
  }

  confirmRename(){
	this.chat.name = this.tmp_name;
	this.chatService.saveChat(this.chat);
	this.chat_renaming = 0;
  }

  denyRename(){
	this.tmp_name = "";
	this.chat_renaming = 0;
  }
  
  leave(){
	this.chatService.leaveChat(this.chat);
  }

}
