import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistsHolderComponent } from './playlists-holder.component';

describe('PlaylistsHolderComponent', () => {
  let component: PlaylistsHolderComponent;
  let fixture: ComponentFixture<PlaylistsHolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistsHolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistsHolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
