import { Component, OnInit, Input } from '@angular/core';
import { Video } from '../../data/video';
import { VideoService } from '../../services/video/video.service'

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  @Input()
  video: Video;
  @Input()
  type: string;
  
  info_visible = 0;
  constructor(private videoService: VideoService) { }

  ngOnInit() {
  }

  getVideoClass(){
	if (this.videoService.player_video &&
		this.video == this.videoService.player_video){
		return "video-playing";
	}
	else{
		return "video";
	}
  }
  
  getVideoButtonClass(){
	if (this.videoService.player_video &&
		this.video == this.videoService.player_video){
		return "video-button-active";
	}
	else{
		return "video-button";
	}
  }

  getInfoButtonClass(){
	if (this.info_visible){
		return "video-button-active";
	}
	else{
		return "video-button";
	}
  }

  play() {
	this.videoService.setPlayerVideo(this.video);
  }

  info(){
	this.info_visible = (this.info_visible + 1) % 2;
  }

  delete(){
	this.videoService.removeVideoFromPlaylist(this.video);
  }

  add(){
	this.videoService.addVideoToPlaylist(this.video);
  }
}
