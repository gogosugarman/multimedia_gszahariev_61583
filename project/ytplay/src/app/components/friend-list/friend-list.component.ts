import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login/login.service'
import { Friend } from '../../data/friend';

@Component({
  selector: 'app-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.css']
})
export class FriendListComponent implements OnInit {
  
  constructor(private loginService: LoginService) { }
 
  ngOnInit() {

  }
  friends_visible = 0;
  search_text = "";

  toggleFriends(){
	this.friends_visible = (this.friends_visible + 1) % 2;
  }

  search(keyEvent: KeyboardEvent){
	if(keyEvent.keyCode === 13){
		this.loginService.searchForFriend(this.search_text.toString());
		this.search_text = "";
	}
  }
}
