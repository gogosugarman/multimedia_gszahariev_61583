import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaylistHolderComponent } from './playlist-holder.component';

describe('PlaylistHolderComponent', () => {
  let component: PlaylistHolderComponent;
  let fixture: ComponentFixture<PlaylistHolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaylistHolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistHolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
