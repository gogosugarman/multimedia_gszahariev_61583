import { Component, OnInit } from '@angular/core';
import { VideoService } from '../../services/video/video.service'

@Component({
  selector: 'app-playlist-holder',
  templateUrl: './playlist-holder.component.html',
  styleUrls: ['./playlist-holder.component.css']
})
export class PlaylistHolderComponent implements OnInit {
  
  constructor(private videoService: VideoService) { }

  ngOnInit() {
  }

}
