import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login/login.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login_visible = 0;
  error_text = '';  
 
  constructor(private loginService: LoginService) { }

  ngOnInit() {
    this.loginService.login_complete.subscribe( value => {
    	if (value === true) {
		this.togglePopup();
    	}
    });
  }

  login() {
	this.loginService.login();
  }

  logout() {
	this.loginService.logout();
  }


  togglePopup() {
	if(this.login_visible){
		this.login_visible = 0;
		this.error_text = '';
		}
	else {
		this.login_visible = 1;
	}
  }

  onKeyLogin(keyEvent: KeyboardEvent){
	if (keyEvent.keyCode === 13){
		this.login();
	}
  }
}
