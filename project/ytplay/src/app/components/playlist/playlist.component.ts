import { Component, OnInit, Input } from '@angular/core';
import { Playlist } from '../../data/playlist';
import { Video } from '../../data/video';
import { PlaylistService } from '../../services/playlist/playlist.service'

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {

  @Input()
  playlist: Playlist;

  constructor(private playlistService: PlaylistService) { }

  ngOnInit() {
  }
  
  getPlaylistClass(){
	if (this.playlist == this.playlistService.current_playlist){
		return "playlist-selected";
	}
	else{
		return "playlist";
	}	
  }

  select(){
	this.playlistService.selectPlaylist(this.playlist);
  }

  delete(){
	this.select();
	this.playlistService.deletePlaylist(this.playlist);
  }

  play(){
	this.select();
	this.playlistService.playSelectedPlaylist();
  }
}
