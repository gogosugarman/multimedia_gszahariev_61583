import { Component, OnInit } from '@angular/core';
import { Chat } from '../../data/chat';
import { Friend } from '../../data/friend';
import { ChatService } from '../../services/chat/chat.service'
import { LoginService } from '../../services/login/login.service'
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.css']
})
export class ChatListComponent implements OnInit {

  constructor(private dragulaService: DragulaService,
	      private chatService: ChatService,
	      private loginService: LoginService) {
	dragulaService.destroy('FRIENDS');
	let frienddrag = dragulaService.createGroup('FRIENDS', {
		moves: function(el,source , handler, sibling){
			return source.id == 'friends.list.id';
		},
		copy: true,
		copyItem: (friend: Friend) => {
        		return JSON.parse(JSON.stringify(friend));
		},
		accepts: function(el, target, source, sibling){
			let children = target.children;
			for (let i=0; i < children.length; i++){
				console.log(children[i].children[0].children[0]);
				if (children[i].children[0].children[0].getAttribute("data-id") == el.children[0].children[0].getAttribute("data-id")){
					return false;
				}
			}
			return true;
		}
	});
	frienddrag.drake.on('drop',
		(el, target, source, sibling) => {
			chatService.saveCurrentChat();
		}
	);	
 }


  ngOnInit() {
  }

  chat_visible = 0;
  message_box = ""; 
  tmp_chat: Chat;

  getChatClass(chat: Chat){
	if(this.chatService.current_chat &&
	   chat.id == this.chatService.current_chat.id){
		return "chat-selected";
	}
	else{
		return "chat";
	}
  }

  getMessageClass(message){
	if (message.poster == this.loginService.currentUser.name){
		return "chat-message";
	}
	else{
		return "chat-message-other";
	}
  }

  getMessageNameClass(message){
	if (message.poster == this.loginService.currentUser.name){
		return "chat-message-name";
	}
	else{
		return "chat-message-name-other";
	}
  }

  newChat(){
	let me = new Friend({
		id: this.loginService.currentUser.id,
		name: this.loginService.currentUser.name,
		online: 1
	});
	let nc = {name: "new chat", "chaters": [me], "messages": [], "id": "0"};
	this.tmp_chat = new Chat(nc);
	this.chatService.current_chat = this.tmp_chat;
	this.saveNewChat();
  }

  saveNewChat(){
	this.chatService.createChat(this.tmp_chat);
	this.tmp_chat = undefined;
	if (this.chatService.chats.length){
		this.chatService.current_chat = this.chatService.chats[0];
	}
	else{
		this.chatService.current_chat = undefined;
	}
  }

  toggleChat(){
	this.chat_visible = (this.chat_visible + 1) % 2;
	this.chatService.update();
	
  }

  setCurrentChat(chat: Chat){
	this.chatService.setCurrentChat(chat);
  }
  
  postMessage(){
	this.chatService.postMessage(this.message_box);
	this.message_box = "";
  }

  
}
