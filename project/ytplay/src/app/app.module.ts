import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxYoutubePlayerModule } from 'ngx-youtube-player';
import { HttpClientModule } from '@angular/common/http';
import { DragulaModule, DragulaService } from 'ng2-dragula';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { FrameComponent } from './components/frame/frame.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { SearchHolderComponent } from './components/search-holder/search-holder.component';
import { VideoComponent } from './components/video/video.component';
import { PlaylistHolderComponent } from './components/playlist-holder/playlist-holder.component';
import { PlayerComponent } from './components/player/player.component';
import { RelatedHolderComponent } from './components/related-holder/related-holder.component';
import { FriendListComponent } from './components/friend-list/friend-list.component';
import { FriendComponent } from './components/friend/friend.component';
import { PlaylistsHolderComponent } from './components/playlists-holder/playlists-holder.component';
import { PlaylistComponent } from './components/playlist/playlist.component';
import { ChatComponent } from './components/chat/chat.component';
import { ChatListComponent } from './components/chat-list/chat-list.component';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FrameComponent,
    SearchBarComponent,
    SearchHolderComponent,
    VideoComponent,
    PlaylistHolderComponent,
    PlayerComponent,
    RelatedHolderComponent,
    FriendListComponent,
    FriendComponent,
    PlaylistsHolderComponent,
    PlaylistComponent,
    ChatComponent,
    ChatListComponent,
    PlaylistListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgxYoutubePlayerModule.forRoot(),
    HttpClientModule,
    DragulaModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
