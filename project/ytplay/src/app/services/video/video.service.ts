import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Video } from '../../data/video';
import { API_KEY } from '../../data/API_KEY'

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  
  constructor(private http: HttpClient) { }
  new_player_video: EventEmitter<boolean> = new EventEmitter();
  search_videos = [];
  related_videos = [];
  playlist_videos = [];
  player_video: Video;
  dragging = 0;

  play(){
	if(this.playlist_videos){
		this.setPlayerVideo(this.playlist_videos[0]);
	}
  }

  setPlayerVideo(video: Video){
	this.player_video = video;
	this.new_player_video.next(true);
	this.searchRelated(video);
  }

  removeVideoFromPlaylist(video: Video){
	let index = this.playlist_videos.indexOf(video);
	if(index != -1){
		this.playlist_videos.splice(index,1);
	}
  }

  addVideoToPlaylist(video: Video){
	let new_vid = JSON.parse(JSON.stringify(video));
	this.playlist_videos.push(new_vid);
  }

  search(search_string){
	let search_addr = "https://www.googleapis.com/youtube/v3/search?part=snippet&order=relevance&maxResults=10&q=" + search_string + "&type=video&key=" + API_KEY;
	this.search_videos = [];
	this.http.get(search_addr).subscribe((search_result) =>{
		for (let video of search_result['items']){
			var new_video: Video = new Video(
				video['id']['videoId'],
				video['snippet']['title'],
				video['snippet']['channelId'],
				video['snippet']['channelTitle'],
				video['snippet']['thumbnails']['default']['url'],
				video['snippet']['thumbnails']['medium']['url'],
				video['snippet']['thumbnails']['high']['url'],
				video['snippet']['thumbnails']['default']['width'],
				video['snippet']['thumbnails']['default']['height'],
				video['snippet']['thumbnails']['medium']['width'],
				video['snippet']['thumbnails']['medium']['height'],
				video['snippet']['thumbnails']['high']['width'],
				video['snippet']['thumbnails']['high']['height'],
				video['snippet']['description']
				);
			this.search_videos.push(new_video);
		}
	})
  }

  searchRelated(video: Video){
  	this.related_videos = [];
	let id = video['id'];
	let search_addr = "https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=10&relatedToVideoId=" + id + "&type=video&key=" + API_KEY;
	this.http.get(search_addr).subscribe((search_result) =>{
		for (let video of search_result['items']){
			var new_video: Video = new Video(
				video['id']['videoId'],
				video['snippet']['title'],
				video['snippet']['channelId'],
				video['snippet']['channelTitle'],
				video['snippet']['thumbnails']['default']['url'],
				video['snippet']['thumbnails']['medium']['url'],
				video['snippet']['thumbnails']['high']['url'],
				video['snippet']['thumbnails']['default']['width'],
				video['snippet']['thumbnails']['default']['height'],
				video['snippet']['thumbnails']['medium']['width'],
				video['snippet']['thumbnails']['medium']['height'],
				video['snippet']['thumbnails']['high']['width'],
				video['snippet']['thumbnails']['high']['height'],
				video['snippet']['description']
				);
			this.related_videos.push(new_video);
		}
	})

  }

  playerNextVideo(){
	let next = 0;
	for (let video of this.playlist_videos){
		if (next){
			this.setPlayerVideo(video);
			return;
		}
		if (video == this.player_video){
			next = 1;
		}
	}
	if (next){
		this.setPlayerVideo(this.playlist_videos[0]);
		return;
	}
  }

  playerPreviousVideo(){
	let prev_vid = this.playlist_videos[this.playlist_videos.length - 1];
	for (let video of this.playlist_videos){
		if (video == this.player_video){
			this.setPlayerVideo(prev_vid);
			return;
		}
		prev_vid = video;
	}
  }
}
