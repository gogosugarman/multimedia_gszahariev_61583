import { Injectable, EventEmitter } from '@angular/core';
import { User } from '../../data/user';
import { Friend } from '../../data/friend';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  currentUser: User = {
		id: -1,
		name: '',
		password: '',
		loged_in: 0,
		auth_token: '',
		friends: [],
		invited_friends: [],
		requested_friends: []
	};
  error_text = "";
  login_complete: EventEmitter<boolean> = new EventEmitter();
  friend_search_result: Array<Friend> = [];

  login() {
	let url = "http://myyoutube.com:4200/api/v1/users/login?name=" 
		+ this.currentUser.name 
		+ "&password=" 
		+ this.currentUser.password;
	url = encodeURI(url);
	this.http.get(url).subscribe((response) =>{
		console.log(response);
		if(response["name"]){
			this.currentUser = new User(response);
			this.error_text = "";
			this.login_complete.next(true);
		}
		else{	
			this.error_text = "Invalid name or password."
		}
	});
  }

  addFriend(new_friend: Friend){
	for (let friend of this.currentUser.friends){
		if (new_friend.id == friend.id){
			return;
		}
	}
	let index = 0;
	for (let friend of this.currentUser.requested_friends){
		if (new_friend.id == friend.id){
			this.currentUser.friends.push(new_friend);
			this.currentUser.requested_friends.splice(index,1);
			this.update();
			return;
		}
		index = index + 1;
	}
	for (let friend of this.currentUser.invited_friends){
		if (new_friend.id == friend.id){
			return;
		}
	}
	this.currentUser.invited_friends.push(new_friend);
	this.update();
  }

  logout() {
	this.currentUser = {
			id: -1,
			name: '',
			password: '',
			loged_in: 0,
			auth_token: '',
			friends: [],
			invited_friends: [],
			requested_friends: []
		};
  }

  removeFriend(friend: Friend){
	let index = this.currentUser.friends.map(
		function(e) { return e.id; }).indexOf(friend.id);
	if(index != -1){
		this.currentUser.friends.splice(index, 1);
	}
  	this.denyFriendRequest(friend);
	this.update();
  }
  
  uninviteFriend(friend: Friend){
	let index = this.currentUser.invited_friends.map(
		function(e) { return e.id; }).indexOf(friend.id);
	if(index != -1){
		this.currentUser.invited_friends.splice(index, 1);
	}
	this.update();
  }

  searchForFriend(search_text: String){
	let url = "http://myyoutube.com:4200/api/v1/users/friendsearch?" 
		+ "search=" 
		+ search_text;
	url = encodeURI(url);
	this.http.get(url).subscribe((response) =>{
		console.log(response);
		if(response["search_result"]){
			this.friend_search_result = response["search_result"];
		}
	});
  }

  denyFriendRequest(friend){
	let url = "http://myyoutube.com:4200/api/v1/users/denyfriend";
	let data = {
		"friend": JSON.stringify(friend),
		"user": JSON.stringify(this.currentUser)
		}
	url = encodeURI(url);
	this.http.put(url, data).subscribe((response) =>{
		console.log(response);
		if(response["result"] && response["result"] == "OK" ){
			this.update();
		}
	});
  }

  update(){
	let url = "http://myyoutube.com:4200/api/v1/users/update";
	url = encodeURI(url);
	let usr = JSON.stringify(this.currentUser);
	this.http.put(url, {"user": usr}).subscribe((response) =>{
		console.log(response);
	});
	
  }
}
