import { Injectable } from '@angular/core';
import { Chat } from '../../data/chat';
import { Friend } from '../../data/friend';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../../services/login/login.service'

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private http: HttpClient,
	      private loginService: LoginService) { }

  chats: Array<Chat> = [];
  current_chat: Chat = undefined;
 
  setCurrentChat(chat: Chat){
	this.current_chat = chat;
  }

  createChat(chat: Chat){
	let url = "http://myyoutube.com:4200/api/v1/chats/create"
	let usr = JSON.stringify(this.loginService.currentUser);
	let cht = JSON.stringify(chat);
	url = encodeURI(url);
	this.http.put(url, {"user": usr, "chat": cht}).subscribe((response) =>{
		console.log(response);
		this.update();
	});
	this.chats.push(chat);
  };
  
  deleteChat(chat: Chat){
	let usr = JSON.stringify(this.loginService.currentUser);
	let url = "http://myyoutube.com:4200/api/v1/chats/delete?"
		+ "user="
		+ usr
		+ "&chat_id="
		+ chat.id;
	url = encodeURI(url);
	this.http.delete(url).subscribe((response) =>{
		console.log(response);
		this.update();
	});
	this.chats.splice(this.chats.indexOf(chat),1);
  }

  whiteMessage(chat, message){
	this.update();
  };

  update(){
	let usr = JSON.stringify(this.loginService.currentUser);
	let url = "http://myyoutube.com:4200/api/v1/chats/mychats?"
		+ "user="
		+ usr; 
	url = encodeURI(url);
	this.http.get(url).subscribe((response) =>{
		console.log(response);
		if(response["chats"]){
			let new_chats: Array<Chat> = [];
			if(this.current_chat){
				this.current_chat.chaters = [];
				this.current_chat.messages = [];
			}
			for (let chat of JSON.parse(response["chats"])){
				let new_chat = new Chat(chat);
				if (this.current_chat &&
				    new_chat.id == this.current_chat.id){
					this.current_chat = new_chat;
				}
				new_chats.push(new_chat);
			}
			this.chats = new_chats;
		}
	});
  };

  saveCurrentChat(){
	this.saveChat(this.current_chat);
  };

  saveChat(chat: Chat){
	let usr = JSON.stringify(this.loginService.currentUser);
	let cht = JSON.stringify(chat);
	let url = "http://myyoutube.com:4200/api/v1/chats/updatechat"
	url = encodeURI(url);
	this.http.post(url, {"user": usr, "chat": cht})
		.subscribe((response) =>
	{
		console.log(response);
		this.update();
	});
  }

  addFriendToCurrentChat(friend: Friend){
	if (! this.current_chat){
		return;
	}
	if (-1 != this.current_chat.chaters.map(
		(x) => {return x.id;}).indexOf(friend.id)){
		return;
	}
	this.current_chat.chaters.push(friend);
	this.saveCurrentChat();
  }

  postMessage(message: string){
	let usr = JSON.stringify(this.loginService.currentUser);
	let url = "http://myyoutube.com:4200/api/v1/chats/postmessage"
	url = encodeURI(url);
	this.http.post(url, {
				"user": usr,
				"message": message,
				"chat_id": this.current_chat.id})
		.subscribe((response) =>
	{
		console.log(response);
		this.update();
	});
  }

  leaveChat(chat: Chat){
	let index = chat.chaters.map(
		(x)=>{return x.id;})
		.indexOf(this.loginService.currentUser.id);
	if (index == -1){
		return;
	}
	chat.chaters.splice(index, 1);
	if (! chat.chaters){
		this.deleteChat(chat);
		return;
	}
	this.saveChat(chat);
  }
  
}
